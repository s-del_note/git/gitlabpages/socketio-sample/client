"use strict";
addEventListener('load', function () {
    var socket = io('https://5chat.f5.si:3000');
    var changeNameInput = document.querySelector('.change-client-name-input');
    var changeNameButton = document.querySelector('.change-client-name-button');
    var displayNameSpan = document.querySelector('.current-client-name');
    var sendMessageInput = document.querySelector('.send-message-input');
    var sendMessageButton = document.querySelector('.send-message-button');
    var messageList = document.querySelector('.message_list');
    function assertIsNull(arg) {
        if (!arg)
            throw new TypeError();
    }
    assertIsNull(sendMessageInput);
    assertIsNull(sendMessageButton);
    assertIsNull(changeNameInput);
    assertIsNull(changeNameButton);
    assertIsNull(displayNameSpan);
    assertIsNull(messageList);
    changeNameInput.addEventListener('keydown', function (ev) {
        if (ev.isComposing)
            return;
        if (ev.code === 'Enter') {
            changeNameButton.click();
        }
    });
    changeNameInput.addEventListener('input', function () {
        if (!changeNameInput.value) {
            changeNameButton.disabled = true;
            return;
        }
        changeNameButton.disabled = false;
    });
    changeNameButton.addEventListener('click', function () {
        socket.emit('change_name', changeNameInput.value);
        changeNameInput.value = '';
        changeNameButton.disabled = true;
        sendMessageInput.focus();
    });
    sendMessageInput.addEventListener('keydown', function (ev) {
        if (ev.isComposing)
            return;
        if (ev.code === 'Enter') {
            sendMessageButton.click();
        }
    });
    sendMessageInput.addEventListener('input', function () {
        if (!sendMessageInput.value) {
            sendMessageButton.disabled = true;
            return;
        }
        sendMessageButton.disabled = false;
    });
    sendMessageButton.addEventListener('click', function () {
        socket.emit('send_message', sendMessageInput.value);
        sendMessageInput.value = '';
        sendMessageButton.disabled = true;
        sendMessageInput.focus();
    });
    socket.on('update_name', function (newName) {
        displayNameSpan.textContent = newName;
    });
    socket.on('response_message', function (clientName, message) {
        var listItem = document.createElement('li');
        listItem.textContent = clientName + ": " + message;
        messageList.insertBefore(listItem, messageList.firstChild);
    });
});
