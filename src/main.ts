addEventListener('load', () => {
  const socket = io('https://5chat.f5.si:3000');

  const changeNameInput =
      document.querySelector<HTMLInputElement>('.change-client-name-input');
  const changeNameButton =
      document.querySelector<HTMLButtonElement>('.change-client-name-button');
  const displayNameSpan =
      document.querySelector<HTMLSpanElement>('.current-client-name');

  const sendMessageInput =
      document.querySelector<HTMLInputElement>('.send-message-input');
  const sendMessageButton =
      document.querySelector<HTMLButtonElement>('.send-message-button');

  const messageList =
      document.querySelector<HTMLUListElement>('.message_list');

  function assertIsNull(arg: unknown): asserts arg {
    if (!arg) throw new TypeError();
  }

  assertIsNull(sendMessageInput);
  assertIsNull(sendMessageButton);
  assertIsNull(changeNameInput);
  assertIsNull(changeNameButton);
  assertIsNull(displayNameSpan);
  assertIsNull(messageList);

  changeNameInput.addEventListener('keydown', (ev: KeyboardEvent) => {
    if (ev.isComposing) return;
    if (ev.code === 'Enter') {
      changeNameButton.click();
    }
  });

  changeNameInput.addEventListener('input', () => {
    if (!changeNameInput.value) {
      changeNameButton.disabled = true;
      return;
    }
    changeNameButton.disabled = false;
  });

  changeNameButton.addEventListener('click', () => {
    socket.emit('change_name', changeNameInput.value);
    changeNameInput.value = '';
    changeNameButton.disabled = true;
    sendMessageInput.focus();
  });

  sendMessageInput.addEventListener('keydown', (ev: KeyboardEvent) => {
    if (ev.isComposing) return;
    if (ev.code === 'Enter') {
      sendMessageButton.click();
    }
  });

  sendMessageInput.addEventListener('input', () => {
    if (!sendMessageInput.value) {
      sendMessageButton.disabled = true;
      return;
    }
    sendMessageButton.disabled = false;
  });

  sendMessageButton.addEventListener('click', () => {
    socket.emit('send_message', sendMessageInput.value);
    sendMessageInput.value = '';
    sendMessageButton.disabled = true;
    sendMessageInput.focus();
  });

  socket.on('no_secure_alert', (message: string) => {
    alert(message);
  });

  socket.on('update_name', (newName: string) => {
    displayNameSpan.textContent = newName;
  });

  socket.on('response_message', (clientName: string, message: string) => {
    const listItem = document.createElement('li');
    listItem.textContent = `${clientName}: ${message}`;
    messageList.insertBefore(listItem, messageList.firstChild);
  });
});
